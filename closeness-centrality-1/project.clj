(defproject closeness-centrality "0.1.0-SNAPSHOT"
  :description "Calculate closeness centrality of a social network"
  :url "http://www.google.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :main closeness-centrality.core)
