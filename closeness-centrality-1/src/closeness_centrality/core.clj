(ns closeness-centrality.core)
(use 'clojure.java.io)
(use 'clojure.set)
(require '[clojure.string :as str])
(require '[clojure.data :as dt])

(def MAX Integer/MAX_VALUE)

(defn readfile
	"Read a file line by line, reducing with a function"
	[file f]
  (with-open [rdr (reader file)]
  (reduce f nil (line-seq rdr)))) ;; now I see I could have passed a empty hashmap

(defn make-graph
	"Make a graph based on strings in the format '<vertex1> <vertex2>'"
	[coll line]
	(let [[f s] (str/split line #" ")]
		(if (not coll)
			(hash-map f (list s) s (list f))
			(conj coll (hash-map f (conj (get coll f) s) s (conj (get coll s) f)))
		)
		))

(defn make-djikistra-ini
	"make hash to initiliaze djikistra. distances should be maxed out"
	[coll k]
	(conj coll (hash-map k MAX)))

(defn process-distance
	"process new distances"
	[vertexes distances distance]
	(conj distances 
		;; Let's update the distance, if we found a better path
		(reduce #(if (< (get distances %2) distance) (assoc %1 %2 (get distances %2)) (assoc %1 %2 distance)) {} vertexes)))


(defn djikistra
	"process djikistra given distances set infinite, except for one that should be 0"
	[graph distances vertexes]
		;; find the closest vertex to keep djkistraing
		(let [closer (reduce #(if (< (get distances %2) (get distances %1)) %2 %1) (first vertexes) (rest vertexes))]
			;; calculate new distances for neighbors, using the increment by 1 as the distance between vertexes
			(let [new-distances (process-distance (get graph closer) distances (inc (get distances closer)))]
				;; take away processed vertex
				(let [except-closer (filter #(not= %1 closer) vertexes)]
					;; find vertexes that changed and make sure to include them in the vector of vertexes to be processed
					(let [vertexes-changed (map #(first %1) (first (dt/diff new-distances distances)))]
						(let [new-vertexes (union vertexes-changed except-closer)]
							(if (= 0 (count new-vertexes)) ;; if we still got vertexes to process, let's call djikistra again
								new-distances
								(djikistra graph new-distances new-vertexes)	
							)
						)
					)
				)
			)
		)
	)

(defn get-distance-sum
	"Given a graph, return each vertex sum of distances from all other vertexes"
	[graph]
	(let [djikistra-ini (reduce make-djikistra-ini {} (keys graph))
				keys-graph (keys graph)]
		(map 
				(fn [vertex] (vector vertex 
					(reduce 
						#(+ %1 (last %2)) 
						0 
						(djikistra graph (conj djikistra-ini {vertex 0}) keys-graph))
					)
				)
				keys-graph)
		)
	)

(defn invert-value
	[kv]
		(vector (first kv) (/ 1 (last kv)))
	)


(defn -main [& args]
	(let [graph (readfile "./edges.txt" make-graph)]
		(println (reverse (sort-by last (map invert-value (get-distance-sum graph)))))
	)
)
