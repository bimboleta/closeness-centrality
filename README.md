# README #

## closeness-centrality-1 ##

Contains first part of the task.
You can run it by typing "**lein run**" inside its folder.

"**lein run**" should output the scores of the vertexes given the edges from edges.txt.
The output is a sorted array of pairs. Each pair represents the vertex and its score in closeness centrality.

"**lein test**" will execute its tests.
There are just a few tests here. Most of them are placed at the second part of the task,
which includes everything of the first part.

## closeness-centrality-2 ##

Contains the second and third part of the task.
You can run it by typing "**lein ring server**" inside its folder.
"**lein test**" will execute its test.

The routes available when you use "**lein ring server**" are:

### POST /add-edge ###
Route for adding an edge

The post should follow "**Content-Type: application/x-www-form-urlencoded**" rules

The post MUST include "**vertex-one**" and "**vertex-two**" text parameters in order to create an edge

No validation is done

Example:

Header: **Content-Type: application/x-www-form-urlencoded**

Body: **vertex-one=1&vertex-two=2**

### POST /add-fraud ###
Route for adding a fraud

Same rules of the /add-edge route apply here

The post MUST include "**fraudulent-vertex**" text parameter and it should exist before the rank is rendered,
otherwise the condition of fraudulent vertex will be applied to some other unexpected vertex.

No validation is done

Example:

Header: **Content-Type: application/x-www-form-urlencoded**

Body: **fraudulent-vertex=1**

### GET /rank ###
Renders a cool HTML table showing the output given by the inputs from /add-edge and /add-fraud

The processing time is **N^3**, with N the amount of vertexes inputed