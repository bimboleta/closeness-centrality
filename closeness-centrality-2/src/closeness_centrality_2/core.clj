(ns closeness-centrality-2.core)
(use 'clojure.java.io)
(use 'clojure.set)
(require '[clojure.string :as str])
(require '[clojure.data :as dt])

(def MAX Integer/MAX_VALUE)

(defn readfile
	"Read a file line by line, reducing with a function"
	[file f]
  (with-open [rdr (reader file)]
  (reduce f nil (line-seq rdr)))) ;; now I see I could have passed a empty hashmap instead of nil

(defn make-graph
	"Make a graph based on strings in the format '<vertex1> <vertex2>'"
	[coll line]
	(let [[f s] (str/split line #" ")]
		(if (not coll)
			(hash-map f (list s) s (list f))
			;; this adds vertex B to A's list and also adds vertex A to B's list
			(conj coll (hash-map f (conj (get coll f) s) s (conj (get coll s) f)))
		)
		))

(defn make-dijkstra-ini
	"make hash to initiliaze dijkstra. distances should be maxed out"
	[coll k]
	(conj coll (hash-map k MAX)))

(defn process-distance
	"process new distances"
	[vertexes distances distance]
	(conj distances 
		;; Let's update the distance, if we found a better path
		(reduce #(if (< (get distances %2) distance) (assoc %1 %2 (get distances %2)) (assoc %1 %2 distance)) {} vertexes)))


(defn dijkstra
	"process dijkstra given distances set infinite, except for one that should be 0"
	[graph distances vertexes]
		;; find the closest vertex to keep djkistraing
		;; future implementations should consider using a heap to reduce complexity (n^2 -> n * log n)
		;; however, the heap I found for clojure had 0 star in github
		;; and doing a heap is kinda out of the scope of this challenge
		(let [closer (reduce #(if (< (get distances %2) (get distances %1)) %2 %1) (first vertexes) (rest vertexes))]
			;; calculate new distances for neighbors, using the increment by 1 as the distance between vertexes
			(let [new-distances (process-distance (get graph closer) distances (inc (get distances closer)))]
				;; take away processed vertex
				(let [except-closer (filter #(not= %1 closer) vertexes)]
					;; find vertexes that changed and make sure to include them in the vector of vertexes to be processed
					(let [vertexes-changed (map #(first %1) (first (dt/diff new-distances distances)))]
						(let [new-vertexes (union vertexes-changed except-closer)]
							(if (= 0 (count new-vertexes)) ;; if we still got vertexes to process, let's call dijkstra again
								new-distances
								(dijkstra graph new-distances new-vertexes)	
							)
						)
					)
				)
			)
		)
	)

(defn get-distance-sum
	"Given a graph, return each vertex sum of distances from all other vertexes"
	[graph]
	(let [dijkstra-ini (reduce make-dijkstra-ini {} (keys graph))
				keys-graph (keys graph)]
				;; this initializes a distance hash-map and also a vector with the keys of the graph
		(map 
			;; let's map through all the vertex keys, doing dijkstra and reducing the result to sum everything
			(fn [vertex] (vector vertex 
				(reduce 
					#(+ %1 (last %2)) 
					0 
					(dijkstra graph (conj dijkstra-ini {vertex 0}) keys-graph))
				)
			)
			keys-graph)
		)
	)

(defn invert-value
	"invert the value of a key-value input"
	[kv]
		(vector (first kv) (/ 1 (last kv)))
	)

(defn generate-rank
	"Given the edges graph, generate a rank of their closeness-centrality"
	[graph]
	(reverse (sort-by last (map invert-value (get-distance-sum graph)))))

(defn ** [x n] (reduce * (repeat n x)))

(defn apply-fraud
	"Given the vertex and a dijkstra of a fraudulent vertex, apply fraud formula"
	[fraud-dijkstra vertex]
	;; The fraud formula is related to distance from the fraudulent vertex to target vector
	(let [factor (- 1 (** (/ 1 2) (get fraud-dijkstra (first vertex))))]
		;; multiply value of target vactor by the factor
		[(first vertex) (* factor (last vertex))])
	)

(defn apply-frauds
	"Apply fraudulent formula in other to punish fraudulent vertexes and his relations"
	[graph frauds]
	(let [ranked (map invert-value (get-distance-sum graph))
		  dijkstra-ini (reduce make-dijkstra-ini {} (keys graph))]
		;; ranked represents a hashmap with each vertex and its score.
		;; the following algorithm does dijkstra again to fraudulent vertexes and remap
		;; the ranked score based on each vertex position.
		(reduce #(let [fraud-dijkstra (dijkstra graph (conj dijkstra-ini {%2 0}) (keys graph))]
				(map (partial apply-fraud fraud-dijkstra) %1)
			) ranked frauds))
	)

(defn -main [& args]
	(let [graph (readfile "./edges.txt" make-graph)]
		;; this gets the distance-sum of all the vertices, invert the results to get closeness and sort it
		(println (generate-rank graph))
	)
)
