(ns closeness-centrality-2.handler
  (:require [compojure.core :refer :all]
  			[cheshire.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.anti-forgery :refer :all]
			[closeness-centrality-2.core :refer :all]
			[cheshire.core :refer :all]
			[net.cgrand.enlive-html :as html]))

;; MUTATABLE DATA
(def edges [])
(def fraudulent-vertexes #{})


;; Helpers for calling the closeness-centrality-2.core library and rendering it
(defn generate-fraudulent-rank
	"Given the edges and fraudulent vertexes, returns a rank of closeness centrality"
	[edgs frauds]
	;; apply-frauds not only generate the rank graph, but also apply the fraud formula given fraudulent vertexes
	(reverse (sort-by last (apply-frauds (reduce make-graph nil edgs) frauds))))

(html/deftemplate main-template "templates/rank.html" []
	;; this will render the output in a pretty nice html table
	[:table [:tr (html/nth-of-type 2)]] (html/clone-for [[k v] (generate-fraudulent-rank edges fraudulent-vertexes)]
												   [:tr [:td (html/nth-of-type 1)]] (html/content (str "Vertex " k ": "))
												   [:tr [:td (html/nth-of-type 2)]] (html/content (str (float v)))))

(defn index-page
	[]
	(main-template))

;; APP ROUTES
(defroutes app-routes
  ;; By default, it's needed to use an anti-forgery-token on POST requests
  ;;(GET "/" [] (generate-string {:csrf-token
  ;;                              *anti-forgery-token*}))
  ;; Disabled anti-forgery-token. so it's not needed anymore
  ;; All the X-CSRF-Token stuff isn't needed anymore
  (GET "/" [] "Read the source file handler.clj for help on the routes")

  ;; Route for adding an edge
  ;; The post should follow "Content-Type: application/x-www-form-urlencoded" rules
  ;; and also include the "X-CSRF-Token" as the route / provides
  ;; The post MUST include vertex-one and vertex-two parameters in order to create an edge
  ;; No validation is done
  (POST "/add-edge" req
  	(let [vertex-one (get (:params req) :vertex-one)
  		  vertex-two (get (:params req) :vertex-two)]
  		(def edges (conj edges (str vertex-one " " vertex-two)))
  		{:status 200
  		:headers {"Content-Type" "application/json; charset=utf-8"}
  		:body (generate-string {:edges (str edges)})})
  	)
  ;; Same rules of the /add-edge route apply here
  ;; The post MUST include fraudulent-vertex and it should exist before the rank is rendered,
  ;; otherwise the condition of fraudulent vertex will be applied to some other unexpected vertex.
  ;; No validation is done
  (POST "/add-fraud" req
  	(let [fraudulent-vertex (get (:params req) :fraudulent-vertex)]
  		(def fraudulent-vertexes (conj fraudulent-vertexes (str fraudulent-vertex)))
  		{:status 200
  		:headers {"Content-Type" "application/json; charset=utf-8"}
  		:body (generate-string {:fraudulent-vertexes (str fraudulent-vertexes)})}))
  ;; Renders a cool table showing the output given by the inputs from /add-edge and /add-fraud
  ;; The processing time is N^3, with N the amount of vertexes inputed
  (GET "/rank" request (index-page))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes (assoc site-defaults :security (assoc (get site-defaults :security) :anti-forgery false))))
