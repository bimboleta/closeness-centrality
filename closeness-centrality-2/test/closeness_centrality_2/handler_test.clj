(ns closeness-centrality-2.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [closeness-centrality-2.handler :refer :all]
            [closeness-centrality-2.core :refer :all]
            [compojure.core :refer :all]
            [cheshire.core :refer :all]))

(def test-graph {"v1" (seq ["v2" "v3" "v4"]) "v2" (seq ["v1"]) "v3" (seq ["v1"]) "v4" (seq ["v1"])})
(def test-graph-score {"v1" (/ 1 3) "v2" (/ 1 5) "v3" (/ 1 5) "v4" (/ 1 5)})
(def test-graph-score-fraudulent {"v1" (/ 1 12) "v2" 0 "v3" 0 "v4" (/ 9 80)})
(deftest closeness-centrality.core
  (testing "make-graph"
    (is (= (make-graph nil "Hey There") {"There" '("Hey") "Hey" '("There")})))

  (testing "make-dijkstra-ini"
    (is (= (make-dijkstra-ini {} "Vertex 1") {"Vertex 1" Integer/MAX_VALUE})))

  (testing "process-distance"
    (is (= (process-distance #{"v1" "v289"} {"v1" 389 "v2" 100 "v289" 400} 289) {"v1" 289 "v2" 100 "v289" 289})))

  (testing "dijkstra"
    (is (= (dijkstra test-graph 
                      {"v1" 0 "v2" Integer/MAX_VALUE "v3" Integer/MAX_VALUE "v4" Integer/MAX_VALUE}
                      #{"v1" "v2" "v3" "v4"})
            {"v1" 0 "v2" 1 "v3" 1 "v4" 1})))

  (testing "get-distance-sum"
    (is (= (last (first (get-distance-sum test-graph))) 3)) ;; stands for v1
    (is (= (last (first (rest (get-distance-sum test-graph)))) 5)) ;; stands for v2
    (is (= (last (first (rest (rest (get-distance-sum test-graph))))) 5)) ;; stands for v3
    (is (= (last (last (get-distance-sum test-graph))) 5))) ;; stands for v4

  (testing "invert-value"
    (is (= (invert-value ["v1" 3]) ["v1" (/ 1 3)])))

  (testing "generate-rank"
    (is (reduce #(and %1 (= (last %2) (get test-graph-score (first %2)))) true (generate-rank test-graph))))

  (testing "**"
    (is (= (** 2 10) 1024)))

  (testing "apply-fraud"
    (is (= (apply-fraud (dijkstra test-graph
                         {"v1" Integer/MAX_VALUE "v2" 0 "v3" Integer/MAX_VALUE "v4" Integer/MAX_VALUE}
                         #{"v1" "v2" "v3" "v4"}) ["v1" (/ 1 3)]) ["v1" (/ 1 6)])))

  (testing "apply-frauds"
    (is (reduce #(and %1 (= (last %2) (get test-graph-score-fraudulent (first %2)))) true (apply-frauds test-graph #{"v2" "v3"}))))

  (testing "generate-fraudulent-rank"
    (is (= (first (generate-fraudulent-rank ["v1 v2" "v1 v3" "v1 v4"] #{"v2" "v3"})) ["v4" (/ 9 80)]))
    (is (= (first (rest (generate-fraudulent-rank ["v1 v2" "v1 v3" "v1 v4"] #{"v2" "v3"}))) ["v1" (/ 1 12)])))
    ;; the order of v2 and v3 doesn't matter as both are equal to 0 at the ending of the rank

  )

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "Read")))) ;; it should contain Read stuff

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404))))
  
  ;; the next 3 tests verify /add-edge route is working correctly
  (testing "add-edge route"
    (let [response (app (-> (mock/request :post "/add-edge")
                  (mock/body {"vertex-one" "Eduardo" "vertex-two" "Henrique"})))
          fake-response {:body {:edges "[\"Eduardo Henrique\"]"}}]
        (is (= (:edges (parse-string (:body response) true)) (:edges (:body fake-response))))))
  (testing "add-edge route, different edge"
    (let [response (app (-> (mock/request :post "/add-edge")
                  (mock/body {"vertex-one" "Eduardo" "vertex-two" "Marcos"})))
          fake-response {:body {:edges "[\"Eduardo Henrique\" \"Eduardo Marcos\"]"}}]
        (is (= (:edges (parse-string (:body response) true)) (:edges (:body fake-response))))))
  (testing "add-edge route, different edge"
    (let [response (app (-> (mock/request :post "/add-edge")
                  (mock/body {"vertex-one" "Eduardo" "vertex-two" "Laura"})))
          fake-response {:body {:edges "[\"Eduardo Henrique\" \"Eduardo Marcos\" \"Eduardo Laura\"]"}}]
        (is (= (:edges (parse-string (:body response) true)) (:edges (:body fake-response))))))

  ;; Testing rank output from these
  ;; Henrique, Marcos and Laura should have output value of 0.2, as the sum of all distances should be 5
  ;; In the other hand, Eduardo, very popular kid, should have output value of 0.33, as the sum of all distances should be 3
  (testing "seeing rank output after adding some edges"
    (let [response (app (mock/request :get "/rank"))]
      (let [match033 (re-find #"0\.33" (str response))
            match02 (re-find #"0\.2" (str response))
            match04 (re-find #"0\.4" (str response))]
        (is (not= nil match033))
        (is (not= nil match02))
        (is (= nil match04))))) ;; there should not exist a match of 0.4

  ;; the next test should add someone as fraudulent
   (testing "add-fraud route"
     (let [response (app (-> (mock/request :post "/add-fraud")
                             (mock/body {"fraudulent-vertex" "Laura"})))]
        (is (= (:fraudulent-vertexes (parse-string (:body response) true)) "#{\"Laura\"}"))))

   ;; Once Laura is considered fraudulent
   ;; Laura must have score of 0
   ;; Eduardo must have his score halfed, resulting in 0.166
   ;; Marcos and Henrique must have their score multiplied by 0.75, resulting in 0.15
   ;; Let's check these values at the rank output
   (testing "seeing rank output after adding a fraudulent vertex"
    (let [response (app (mock/request :get "/rank"))]
      (let [match00 (re-find #"0\.0" (str response))
            match0166 (re-find #"0\.166" (str response))
            match015 (re-find #"0\.15" (str response))]
        (is (not= nil match00))
        (is (not= nil match0166))
        (is (not= nil match015)))))

   ;; the next test should add someone as fraudulent
   (testing "add-fraud route"
     (let [response (app (-> (mock/request :post "/add-fraud")
                             (mock/body {"fraudulent-vertex" "Marcos"})))]
        (is (or (= (:fraudulent-vertexes (parse-string (:body response) true)) "#{\"Marcos\" \"Laura\"}")
                (= (:fraudulent-vertexes (parse-string (:body response) true)) "#{\"Laura\" \"Marcos\"}")))))

   ;; Once Marcos is considered fraudulent
   ;; Marcos must have score of 0
   ;; Eduardo must have his score halfed, resulting in 0.083
   ;; Henrique must have his score multiplied by 0.75, resulting in 0.1125
   ;; Let's check these values at the rank output
   (testing "seeing rank output after adding a fraudulent vertex"
    (let [response (app (mock/request :get "/rank"))]
      (let [match00 (re-find #"0\.0" (str response))
            match0083 (re-find #"0\.083" (str response))
            match01125 (re-find #"0\.1125" (str response))]
        (is (not= nil match00))
        (is (not= nil match0083))
        (is (not= nil match01125)))))
  )
